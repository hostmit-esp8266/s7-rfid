#include <Arduino.h>
#include <FS.h>
#include <WiFiUdp.h>
#include <ESP8266WiFi.h>


#include "credentials.h"
#include "config.h"
#include "logger.h"
#include "ota.h"
#include "gpioswitch.h"
#include "sysinfo.h"
#include "web.h"
#include "timetools.h"
#include "fstools.h"

#include "rfid.h"
#include "loggerudp.h"
#include "blynkmy.h"


void setup()
{

    Serial.begin(115200);
    delay(5000);
    Serial.println("Starting...");
    if (!SPIFFS.begin())
    {
        Serial.println("SPIFFS.begin() failed, will try to format...");
        if (SPIFFS.format())
        {
            Serial.println("SPIFFS.format() success...");
            if (!SPIFFS.begin())
            {
                Serial.println("SPIFFS.begin() failed...");
            }
            else
            {
                Serial.println("SPIFFS.begin() success...");
            }
        }
        else
            Serial.println("SPIFFS.format() fail...");
    }
    else
    {
        Serial.println("SPIFFS.begin() success...");
    }

    Logger::syslog("Setup Started");
    gotIpEventHandler = WiFi.onStationModeGotIP([](const WiFiEventStationModeGotIP &event) {
        wifiConnectTime = millis();
        wifiConnectCount++;
        wifiFirstConnected = true;
        Logger::syslog("Station connected, IP: " + WiFi.localIP().toString() + ", wifiConnectCount: " + String(wifiConnectCount));
    });

    disconnectedEventHandler = WiFi.onStationModeDisconnected([](const WiFiEventStationModeDisconnected &event) {
        Logger::syslog("Station disconnected. wifiConnectTime: " + Timetools::msToFormattedInterval(millis() - wifiConnectTime));
    });

    WiFi.mode(WIFI_STA);
    WiFi.begin(WIFI_DEFAULT_SSID, WIFI_DEFAULT_PASS);
    while (WiFi.waitForConnectResult() != WL_CONNECTED)
    {
        Logger::syslog("WiFi Connect Failed! Not Rebooting...");
        delay(1000);
    }
    Web::setup();
    Ota::setup();
    builtInLed.start(-1, 50);
    Timetools::setup();
    BlynkMy::setup();
    Rfid::setup();
    LoggerUdp::setup();
}

void loop()
{
 if (espResetRequest)
    {
        Logger::syslog("espResetRequest request received by loop(), will reset after delay:" + String(ESPRESETDELAY));
        delay(ESPRESETDELAY);
        ESP.reset();
    }
    if (espRestartRequest)
    {
        Logger::syslog("espRestartRequest request received by loop(), will reset after delay:" + String(espRestartRequest));
        delay(ESPRESTARTDELAY);
        ESP.restart();
    }

    Ota::handle();
    Gpioswitch::globalHandle();
    Sysinfo::loopStatHandle();
    Timetools::handle();
    BlynkMy::handle();
    LoggerUdp::handle();
    Rfid::handle();
}