#ifndef LOGGER_UDP_H
#define LOGGER_UDP_H

#include <ArduinoJson.h>
#include <ESP8266WiFi.h>
#include "ESPAsyncUDP.h"
#include "config.h"
#include "credentials.h"

struct LOGGER_UDP_LOG_QUEUE_STRUCT {
    String msg;
    uint level;
};



class LoggerUdp {

    public:

    static bool setupCompleted;
    static AsyncUDP udp;
    static std::vector<LOGGER_UDP_LOG_QUEUE_STRUCT> logQueueV;
    static IPAddress gelfIp;

    static void setup() {
        int err = WiFi.hostByName(LOGGER_UDP_HOSTNAME,gelfIp);
        if (err == 1 ) {
            selfLog("GELF hostname: " + String(LOGGER_UDP_HOSTNAME) + ", resolved to: " + String(gelfIp.toString()));
            selfLog(gelfIp.toString());
            if(udp.connect(IPAddress(gelfIp), LOGGER_UDP_PORT)) {
                selfLog("Gelf UDP connected to " + gelfIp.toString() + ":" + String(LOGGER_UDP_PORT));
                setupCompleted = true;
            }
            
            
        } else {
            selfLog("Unable to resolve hostname: " + String(LOGGER_UDP_HOSTNAME));
        }
        
    }
    static void log (String msg, uint level = LOG_INFO) {
        if (msg.length() > LOGGER_UDP_MAX_LENGTH) msg.substring(0,LOGGER_UDP_MAX_LENGTH);
        StaticJsonDocument<LOGGER_UDP_MAX_LENGTH*2> doc;
        doc["host"] = GENERAL_SOURCE;
        doc["type"] = GENERAL_TYPE;
        doc["name"] = GENERAL_NAME;
        // doc["version"] = "1.1";
        doc["short_message"] = msg;
        doc["level"] = level;
        String r;
        serializeJson(doc, r);
        udp.print(r.c_str());
    }

    static void selfLog(String msg, uint level = LOG_INFO) {
        Serial.println(msg);
    }

    
    static void logToQueue(String msg, uint level = LOG_INFO)
    {
        while (logQueueV.size() > LOGGER_UDP_QUEUE_MAX_SIZE) logQueueV.erase(logQueueV.begin());
        logQueueV.push_back({msg,level});
    }

    static void logQueueProcess() {
        for (auto i: logQueueV) {
            log(i.msg,i.level);
        }
        logQueueV.clear();
    }

    static void handle() {
        logQueueProcess();
    }

};
bool LoggerUdp::setupCompleted = false;
AsyncUDP LoggerUdp::udp;
std::vector<LOGGER_UDP_LOG_QUEUE_STRUCT> LoggerUdp::logQueueV;
IPAddress LoggerUdp::gelfIp;
#endif