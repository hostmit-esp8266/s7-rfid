#ifndef TIMETOOLS_H
#define TIMETOOLS_H

#include <ESP8266WiFi.h>
#include <TimeLib.h>
#include "config.h"
#include "loggerudp.h"

class Timetools
{
public:
    static WiFiUDP udp;
    // static time_t getNtpTime();
    // static void sendNTPpacket(IPAddress &address);
    static byte packetBuffer[NTP_PACKET_SIZE]; //buffer to hold incoming & outgoing packets
    static time_t upFrom;

    static void setup()
    {
        udp.begin(NTP_LOCAL_PORT);
        setSyncProvider(getNtpTime);
        setSyncInterval(NTP_SYNC_INTERVAL_SEC);
        if (timeStatus() == timeSet) {
            upFrom = now();
            log("timetools setup completed...",LOG_INFO);
        }
        
    }

    static void handle()
    {
    }

    static time_t getNtpTime()
    {
        IPAddress ntpServerIP; // NTP server's ip address
        while (udp.parsePacket() > 0)
            ; // discard any previously received packets
        //Logger::syslog("Transmit NTP Request");
        // get a random server from the pool
        WiFi.hostByName(NTP_HOST, ntpServerIP);
        //Logger::syslog(String(ntpServerName));
        //Logger::syslog(": ");
        //Logger::syslog(ntpServerIP.toString());
        sendNTPpacket(ntpServerIP);
        uint32_t beginWait = millis();
        while (millis() - beginWait < 1500)
        {
            int size = udp.parsePacket();
            if (size >= NTP_PACKET_SIZE)
            {
                //Logger::syslog("Receive NTP Response");
                udp.read(packetBuffer, NTP_PACKET_SIZE); // read packet into the buffer
                unsigned long secsSince1900;
                // convert four bytes starting at location 40 to a long integer
                secsSince1900 = (unsigned long)packetBuffer[40] << 24;
                secsSince1900 |= (unsigned long)packetBuffer[41] << 16;
                secsSince1900 |= (unsigned long)packetBuffer[42] << 8;
                secsSince1900 |= (unsigned long)packetBuffer[43];
                log("NTP update succeed!", LOG_INFO);
                return secsSince1900 - 2208988800UL + NTP_TIME_ZONE * SECS_PER_HOUR;
            }
        }
        log("No NTP Response :-(",LOG_ERR);
        return 0; // return 0 if unable to get the time
    }

    // send an NTP request to the time server at the given address
    static void sendNTPpacket(IPAddress &address)
    {
        // set all bytes in the buffer to 0
        memset(packetBuffer, 0, NTP_PACKET_SIZE);
        // Initialize values needed to form NTP request
        // (see URL above for details on the packets)
        packetBuffer[0] = 0b11100011; // LI, Version, Mode
        packetBuffer[1] = 0;          // Stratum, or type of clock
        packetBuffer[2] = 6;          // Polling Interval
        packetBuffer[3] = 0xEC;       // Peer Clock Precision
        // 8 bytes of zero for Root Delay & Root Dispersion
        packetBuffer[12] = 49;
        packetBuffer[13] = 0x4E;
        packetBuffer[14] = 49;
        packetBuffer[15] = 52;
        // all NTP fields have been given values, now
        // you can send a packet requesting a timestamp:
        udp.beginPacket(address, 123); //NTP requests are to port 123
        udp.write(packetBuffer, NTP_PACKET_SIZE);
        udp.endPacket();
        log("NTP udp packet sent", LOG_INFO);
    }

    static String formattedDateTimeFromMillis(ulong m) {
        return formattedDateTime(now() - (millis() - m) /1000 );
    }

    static String formattedDateTime(time_t t = now()) {
            return "[" + String(year(t)) + "-" + String(month(t)) + "-" + String(day(t))  + " " + String(hour(t))  + ":" + String(minute(t))  + ":" + String(second(t)) + "]";
    }
    static String msToFormattedInterval(long timeArg = 0)
    {
        String text = "";

        long days = 0;
        long hours = 0;
        long mins = 0;
        long secs = 0;
        timeArg == 0 ? secs = millis() / 1000 : secs = timeArg / 1000;

        //secs = millis() / 1000;      //convect milliseconds to seconds
        mins = secs / 60;            //convert seconds to minutes
        hours = mins / 60;           //convert minutes to hours
        days = hours / 24;           //convert hours to days
        secs = secs - (mins * 60);   //subtract the coverted seconds to minutes in order to display 59 secs max
        mins = mins - (hours * 60);  //subtract the coverted minutes to hours in order to display 59 minutes max
        hours = hours - (days * 24); //subtract the coverted hours to days in order to display 23 hours max
        // //Display results
        // text += "--- Running Time ---\n";
        if (days > 0) // days will displayed only if value is greater than zero
        {
            text += String(days) + " days and ";
        }
        if (hours < 10)
            text += "0";
        text += String(hours) + ":";
        if (mins < 10)
            text += "0";
        text += String(mins) + ":";
        if (secs < 10)
            text += "0";
        text += String(secs);
        return text;
    }

    static String getWebInfo() {
        return "timeStatus: " + String(timeStatus()) + " formattedDateTime " + Timetools::formattedDateTime() 
            + ", upFrom: " + Timetools::formattedDateTime(Timetools::upFrom);
    }

    static void log(String msg, uint level = 6) {
        LoggerUdp::log(msg,level);
    }

};
WiFiUDP Timetools::udp;
byte Timetools::packetBuffer[NTP_PACKET_SIZE];
time_t Timetools::upFrom;
#endif