#ifndef RFID_H
#define RFID_H

#include <Arduino.h>
#include <SPI.h>
#include <MFRC522.h>

#include <ESP8266WiFi.h>
#include <WiFiUdp.h>
#include <WiFiClient.h>
#include <ArduinoJson.h>
#include <ESPAsyncUDP.h>


#include "config.h"
#include "logger.h"
#include "gpioswitch.h"
#include "credentials.h"


#define S7_TO "s7"
#define S7_TO_KEY "to"
#define S7_RFID_KEY "rfid"
#define S7_LISTEN_PORT 3333
#define S7_FROM_KEY "from"
#define S7_FROM "rfid"

#define RFID_READ_DELAY 1000
#define RFID_MAIN_LOOP_DELAY 1000
#define RFID_EXTERNAL_LED 4
#define S7_ZONE_KEY "zone"
#define S7_ZONE_NUMBER 2

#define RFID_NO_DATA_TIMEOUT 3000

#define RFID_RST_PIN 5
#define RFID_SS_PIN 15


class Rfid
{
  public:
    static MFRC522 mfrc522;               // Create MFRC522 instance
    static MFRC522::MIFARE_Key key;       // Prepare key - all keys are set to FFFFFFFFFFFFh at chip delivery from the factory.
    static byte block;
    static byte len;
    static byte buffer[18];
    static ulong rfidCardDetectedMillis;

    static Gpioswitch ledOut;

    //static AsyncUDP udpSend;
    static AsyncUDP udp;

    static ulong stateUpdated;
    static bool updateRequired;
    static int state;

    static String packetReceivedLast;
    static String packetSentLast;


    static void setup()
    {
        for (int i = 0; i < 6; i++)
        {
            key.keyByte[i] = hexStringToInt(String(RFID_KEY[i * 2]) + String(RFID_KEY[i * 2 + 1]));
        }

        SPI.begin();        // Init SPI bus
        mfrc522.PCD_Init(); // Init MFRC522 card
        //reset communication to RC522
        mfrc522.PICC_HaltA();
        mfrc522.PCD_StopCrypto1();

        ledOut.start(-1,500);


        if(udp.listenMulticast(IPAddress(239,1,1,1), S7_LISTEN_PORT)) {
            log("Listening on port:" + String(S7_LISTEN_PORT),LOG_INFO);
            udp.onPacket([](AsyncUDPPacket packet) {
                String packetData = "";
                for (size_t i=0;i<packet.length();i++){
                    packetData += (char)*(packet.data()+i);
                }
                
                if (packetData != packetReceivedLast) {
                    packetReceivedLast = packetData;
                    String m = "UDP Packet Type: ";
                    m += packet.isBroadcast()?"Broadcast":packet.isMulticast()?"Multicast":"Unicast";
                    m += ", From: " + packet.remoteIP().toString() + ":" + String(packet.remotePort()) + ", To: " + packet.localIP().toString() + ":";
                    m += String(packet.localPort()) + ", Length: " + String(packet.length()) + ", Data: " + packetData;
                    //log(m,LOG_DEBUG);
                }
                
                StaticJsonDocument<512> doc;
                DeserializationError error = deserializeJson(doc, packetData);
                if (error)
                {
                    log("Failed to parse JSON.", LOG_ERR);
                    return;
                }
                if (doc[S7_FROM_KEY].isNull() ||  doc[String(S7_ZONE_KEY)+String(S7_ZONE_NUMBER)].isNull())
                {
                    log("Keys S7_FROM_KEY:" + String(S7_FROM_KEY) +  " or S7_ZONE_KEY: " + String(S7_ZONE_KEY) + String(S7_ZONE_NUMBER) + " is missing", LOG_ERR);
                    return;
                }

                if (doc[S7_FROM_KEY].as<String>() != S7_TO) {
                    log("I dont listen to the host: " + String(S7_TO), LOG_ERR);
                    return;
                }

                stateUpdated = millis();
                int newState;
                doc[String(S7_ZONE_KEY)+String(S7_ZONE_NUMBER)].as<bool>() ? newState = 1 : newState = 0;
                if (newState!=state) {
                    log("New State: " + String(newState) + ", state: " + String(state), LOG_INFO);
                    state = newState;
                    stateUpdated = millis();
                    state == 0 ? ledOut.off() : ledOut.on();
                }

                
            });
        }
    }

    static void handle()
    {

        if (millis() - stateUpdated > RFID_NO_DATA_TIMEOUT && state != -1)  {
            state = -1;
            ledOut.start(-1,500);
        }
        readRfid();


    }
    //takes String of HEX characters and tries to convert to INT. Return -1 if failed
    static long hexStringToInt(String s)
    {

        char *p;
        long n = strtol(s.c_str(), &p, 16);
        if (*p != 0)
        {
            return -1;
        }
        else
        {
            return n;
        }
    }

    static void readRfid() {
        if (millis() - rfidCardDetectedMillis > RFID_READ_DELAY) //carefull, we dont return from here
        {
            MFRC522::StatusCode status;
            // Look for new cards, if not found - return
            if (!mfrc522.PICC_IsNewCardPresent())
                return;

            // Select one of the cards
            if (!mfrc522.PICC_ReadCardSerial())
                return;

            //If here - got new Card
            log("**Card Detected:**", LOG_INFO);

            //------------------------------------------- get block value
            status = mfrc522.PCD_Authenticate(MFRC522::PICC_CMD_MF_AUTH_KEY_A, 4, &key, &(mfrc522.uid)); //line 834 of MFRC522.cpp file
            if (status != MFRC522::STATUS_OK)
            {
                log("Authentication failed! Code: " + String(mfrc522.GetStatusCodeName(status)), LOG_ERR);

                rfidCardDetectedMillis = millis();
                //delay(rfidDelay);
                return;
            }

            len = sizeof(buffer);
            status = mfrc522.MIFARE_Read(block, buffer, &len);
            if (status != MFRC522::STATUS_OK)
            {
                log("Reading failed! Code: " + String(mfrc522.GetStatusCodeName(status)), LOG_ERR);
                //probably got RFID module freeze, added in attempt to avoid
                //16.09.2018
                /*
            **Card Detected:**
            Reading failed! Code: The CRC_A does not match.
            **Card Detected:**
            Authentication failed! Code: Error in communication.
            **Card Detected:**
            Reading failed! Code: A buffer is not big enough.
            */
                log("Performing mfrc522.PCD_Reset()", LOG_ERR);
                //mfrc522.PCD_Reset(); //16.09.2018
                mfrc522.PICC_HaltA();
                mfrc522.PCD_StopCrypto1();
                // //27.09.2018. Above doesnt help, module just stops reading
                // mfrc522.PCD_Init(); // Init MFRC522 card

                // //if above doesnt help try insert this BEFORE PCD_init()
                // SPI.end();        // Init SPI bus
                // SPI.begin();        // Init SPI bus
                return;
            }

            String payload_s = "";
            for (uint8_t i = 0; i < 16; i++)
            {
                    String a = String((byte)buffer[i], HEX);
                    while (a.length() < 2)
                        a = "0" + a;
                    payload_s += a;
            }
            log("GOT RFID: " + payload_s, LOG_INFO);
            
            if (WiFi.isConnected()) {
                StaticJsonDocument<512> doc;
                doc[S7_TO_KEY] = S7_TO;
                doc[S7_FROM_KEY] = S7_FROM;
                doc[S7_ZONE_KEY] = S7_ZONE_NUMBER;
                doc[S7_RFID_KEY] = payload_s;
                String p;
                serializeJson(doc, p);
                udp.print(p.c_str());
                log("RFID reading completed packet sent", LOG_INFO);
                packetSentLast = p;
            }
            log("will do delay: " + String(RFID_MAIN_LOOP_DELAY) + " and reset RC522", LOG_INFO);
            mfrc522.PICC_HaltA();
            mfrc522.PCD_StopCrypto1();
            rfidCardDetectedMillis = millis();
        }
    }

    static void log(String msg, uint level = 6)
    {
        if (level <= 6) Logger::syslog(msg);
        LoggerUdp::logToQueue(msg, level);
    }

    static String getWebInfo(){
        String t = "";
        t += "<strong>--- RFID: </strong>";
        t += "Zone #: " + String(S7_ZONE_NUMBER) + "; ";
        t += "State: " + String(state) + "; ";
        t += "Updated: " + String(Timetools::formattedDateTimeFromMillis(stateUpdated)) + "; ";
        t += "\n";
        t += "lastReceivedPacket: " + packetReceivedLast + ";";
        t += "\n";
        t += "lastSentPacket: " + packetSentLast + ";";
        return t;
    }



};

MFRC522 Rfid::mfrc522(RFID_SS_PIN, RFID_RST_PIN); // Create MFRC522 instance
byte Rfid::block = 4;
byte Rfid::len;
ulong Rfid::rfidCardDetectedMillis = 0;
byte Rfid::buffer[18];
MFRC522::MIFARE_Key Rfid::key; // Prepare key - all keys are set to FFFFFFFFFFFFh at chip delivery from the factory.

Gpioswitch Rfid::ledOut(RFID_EXTERNAL_LED);

AsyncUDP Rfid::udp;

ulong Rfid::stateUpdated = 0;
int Rfid::state = -1;
String Rfid::packetReceivedLast = "";
String Rfid::packetSentLast = "";

#endif