#ifndef BLYNKMY_H
#define BLYNKMY_H

#include <Arduino.h>
#include <BlynkSimpleEsp8266.h>
#include "logger.h"
#include "config.h"
#include "sysinfo.h"
#include "credentials.h"

class BlynkMy
{
public:

  static void setup()
  {

    Blynk.config(BLYNK_TOKEN, BLYNK_HOST, BLYNK_PORT);
  }

  static void handle()
  {
    Blynk.run();
  }

  static void log(String msg, uint level = 6)
  {
    Logger::syslog(msg);
  }
};

BLYNK_CONNECTED()
{
  BlynkMy::log("Blynk connected to host: " + String(BLYNK_HOST) + ", port: " + String(BLYNK_HOST) + ", token: " + String(BLYNK_TOKEN), 6);
}

BLYNK_WRITE(InternalPinOTA)
{
  BlynkMy::log("InternalOta update requested");
  auto overTheAirURL = param.asString();
  BlynkMy::log(overTheAirURL);
};

#endif
