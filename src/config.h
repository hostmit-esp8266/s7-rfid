#ifndef CONFIG_H
#define CONFIG_H

#include <Arduino.h>
#include <map>
#include <ESPAsyncTCP.h>
#include <ESPAsyncWebServer.h>
#include "gpioswitch.h"

#define GENERAL_SOURCE "luch2/rfid-house"
#define GENERAL_TYPE "rfid"
#define GENERAL_NAME "rfid-house"


//logger
#define LOGUSESERIAL true
#define LOGFILENAME "syslog.log"
#define LOGLINEMAXLENGTH 1000
#define LOGFILESIZE 10000
#define LOGPREVEXT "prev"


//gpioswitcher
#define ONBOARDLEDPIN 2
Gpioswitch builtInLed(ONBOARDLEDPIN);

//sysinfo
#define LOOPSTATSIZE 10
#define LOOPSTATDURATION 1000


//wifi section
long wifiConnectTime = 0;
long wifiConnectCount = 0;
WiFiEventHandler gotIpEventHandler, disconnectedEventHandler;
bool wifiFirstConnected;

//sys
#define ESPRESTARTDELAY 2000
#define ESPRESETDELAY 2000
bool espRestartRequest = false;
bool espResetRequest = false;

//ntpdate


#define NTP_HOST "0.ua.pool.ntp.org"
#define NTP_LOCAL_PORT 8888
#define NTP_TIME_ZONE 2
#define NTP_PACKET_SIZE 48
#define NTP_SYNC_INTERVAL_SEC 60*60*5

#define LOG_EMERG 0
#define LOG_ALERT 1
#define LOG_CRIT 2
#define LOG_ERR 3
#define LOG_WARNING 4
#define LOG_NOTICE 5
#define LOG_INFO 6
#define LOG_DEBUG 7

#endif