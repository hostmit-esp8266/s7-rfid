#ifndef WEB_H
#define WEB_H

#include <Arduino.h>
#include <ESPAsyncWebServer.h>
#include <BlynkSimpleEsp8266.h>
#include "config.h"
#include "logger.h"
#include "sysinfo.h"
#include "fstools.h"
#include "timetools.h"
#include "rfid.h"
#include "loggerudp.h"

class Web
{
  public:
    static AsyncWebServer server;
    
    static void setup()
    {
        root();
        fs();
        sys();
        server.begin();
    }

  private:
    static void fs()
    {
        server.on("/fs/list", HTTP_GET, [](AsyncWebServerRequest *request) {
            String htmlResponse = Fstools::webListRoot();

            request->send(200, "text/html", htmlResponse);
        });
        server.on("/fs/format", HTTP_GET, [](AsyncWebServerRequest *request) {
            bool result = SPIFFS.format();
            String t = "";
            result == true ? t = "Successfuly formated" : t = "Unable to format";
            request->send(200, "text/plain", t);
        });

        server.on("/fs/read", HTTP_GET, [](AsyncWebServerRequest *request) {

            if (request->hasArg("name"))
            {
                request->send(SPIFFS, request->arg("name"), "text/plain");
            }
            else
            {
                String htmlResponse = "";
                htmlResponse += "<pre>";
                htmlResponse = +"I need file name";
                htmlResponse += "<a href='/fs/list'>Back to fs list</a>\n";
                htmlResponse += "<a href='/'>Back to /</a>\n";
                htmlResponse += "</pre>";
                request->send(200, "text/html", htmlResponse);
            }
        });

        server.on("/fs/delete", HTTP_GET, [](AsyncWebServerRequest *request) {
            String htmlResponse = "";
            htmlResponse += "<pre>";
            if (request->hasArg("name"))
            {
                SPIFFS.remove(request->arg("name")) ? htmlResponse += "File removed...\n" : htmlResponse += "Remove file FAILED...\n";
            }
            else
            {
                htmlResponse = +"I need file name";
            }
            htmlResponse += "<hr>";
            htmlResponse += "<a href='/fs/list'>Back to fs list</a>\n";
            htmlResponse += "<a href='/'>Back to /</a>\n";
            htmlResponse += "</pre>";
            request->send(200, "text/html", htmlResponse);
        });
    }

    static void root()
    {
        server.on("/", HTTP_GET, [](AsyncWebServerRequest *request) {
            String t = "";
            t += "<pre>";
            t += "/fs/{<a href='/fs/list'>list</a>,<a href='/fs/format' onclick=\"return confirm('Are you sure?')\">format</a>}\n";
            t += "\n";
            t += "<strong>--- Available commands are: --- </strong>\n";
            t += "\n";
            t += "/sys/{<a href='/sys/restart' onclick=\"return confirm('Are you sure?')\">restart</a>,<a href='/sys/reset' onclick=\"return confirm('Are you sure?')\">reset</a>}\n";
            t += "\n";
            t += "<strong>--- General info: </strong>" + Sysinfo::getGeneralInfo();
            t += "\n";
            t += "<strong>--- Uptime: </strong>" + Timetools::msToFormattedInterval(millis());
            t += "\n";
            t += Sysinfo::getRamInfo();
            t += Sysinfo::getFsInfo();
            t += Sysinfo::getResetReason();
            t += Sysinfo::getLoopStat();
            t += "<strong>--- Timetools: </strong>" + Timetools::getWebInfo();
            t += "\n";
            t += "<strong>--- WiFi: </strong>" + Sysinfo::getWifi();
            t += "\n";
            t += "<strong>--- Blynk:</strong> connected: " + String(Blynk.connected()) + ", BLYNK_HOST: " + String(BLYNK_HOST) + ", BLYNK_PORT: " + String(BLYNK_HOST) + ", BLYNK_TOKEN: " + BLYNK_TOKEN;
            t += "\n";
            t += "<strong>--- UDPLOGGER:</strong> LOGGER_UDP_HOSTNAME: " + String(LOGGER_UDP_HOSTNAME) + ", LOGGER_UDP_PORT: " + String(LOGGER_UDP_PORT ) + ", LOGGER_UDP_MAX_LENGTH: " + String(LOGGER_UDP_MAX_LENGTH) + ", LOGGER_UDP_QUEUE_MAX_SIZE: " + String(LOGGER_UDP_QUEUE_MAX_SIZE) + ", LoggerUdp:logQueueV.size(): " + String(LoggerUdp::logQueueV.size()) + ", LoggerUpd::gelfIp.toString(): " + LoggerUdp::gelfIp.toString();
            t += "\n";
            t += Rfid::getWebInfo();
            t += "</pre>";
            request->send(200, "text/html", t);
        });
    }
    static void sys()
    {
        server.on("/sys/restart", HTTP_GET, [](AsyncWebServerRequest *request) {
            String htmlResponse = "";
            htmlResponse += "<pre>";
            htmlResponse += "ESP.restart() will be issued after delay:" + String(ESPRESTARTDELAY);
            htmlResponse += "<hr>";
            htmlResponse += "<a href='/'>Back to /</a>\n";
            htmlResponse += "</pre>";
            request->send(200, "text/html", htmlResponse);
            espRestartRequest = true;
        });
        server.on("/sys/reset", HTTP_GET, [](AsyncWebServerRequest *request) {
            String htmlResponse = "";
            htmlResponse += "<pre>";
            htmlResponse += "ESP.reset() will be issued after delay:" + String(ESPRESTARTDELAY);
            htmlResponse += "<hr>";
            htmlResponse += "<a href='/'>Back to /</a>\n";
            htmlResponse += "</pre>";
            request->send(200, "text/html", htmlResponse);
            espResetRequest = true;
        });
    }
};

AsyncWebServer Web::server(80);

#endif